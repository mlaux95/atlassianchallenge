package com.matthewlaux.challenge;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.matthewlaux.challenge.parser.ParserTask;

/**
 * This is my Android solution to the Atlassian programming challenge.
 *
 * ChallengeActivity is the main activity, providing the UI.
 * ParserTask is an AsyncTask that runs all of the Parsers on a chat string.
 * Parser is the common super-interface of all the Parsers.
 * MentionParser extracts mentions from messages.
 * EmoticonParser extracts emoticons from messages.
 * LinkParser extracts links from messages and finds their titles.
 * ParseUtil contains a regex method that all 3 Parsers use.
 *
 * Created by mlaux on 4/1/15.
 */
public class ChallengeActivity extends ActionBarActivity {
  private EditText mChatBox;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // set up UI and add listener to the 'parse' button
    setContentView(R.layout.activity_challenge);
    setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

    mChatBox = (EditText) findViewById(R.id.chat);

    findViewById(R.id.parse).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String chat = mChatBox.getText().toString();
        new ParserTask(ChallengeActivity.this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, chat);
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_challenge, menu);
    return true;
  }

  /**
   * The activity has a simple options menu with one item that populates the text field
   * with an example chat message.
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.action_example) {
      mChatBox.setText(getString(R.string.example));
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
