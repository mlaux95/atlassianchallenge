package com.matthewlaux.challenge.parser;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by mlaux on 3/27/15.
 */
public class MentionParser implements Parser<String> {
  private static final Pattern REGEX = Pattern.compile("@(\\w+)");

  @Override
  public List<String> extractMatches(String chat) {
    // Find occurrences of @ followed by one or more word characters
    // and keep the word characters.
    return ParseUtil.applyRegex(chat, REGEX);
  }

  @Override
  public String getJsonFieldName() {
    return "mentions";
  }
}
