package com.matthewlaux.challenge.parser;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created by mlaux on 3/27/15.
 */
public class LinkParser implements Parser<LinkParser.Link> {
  private static final String LOG_TAG = LinkParser.class.getName();

  private static final Pattern REGEX = Pattern.compile("(https?://[^\\s]+)");

  /** Let's pretend to be a web spider so pages that would normally require login or show an ad
      give us their content. */
  private static final String USER_AGENT = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";

  /** Default title to use if the request times out */
  private static final String NO_TITLE = "";

  /** How many threads we should look for titles with */
  private static final int NUM_THREADS = 4;

  /** Wait at most this many seconds for all titles to come in. */
  private static final long TIMEOUT_SECONDS = 5;

  /** This class will get serialized into JSON for each link. */
  static class Link {
    public String url;
    public String title;

    public Link(String url, String title) {
      this.url = url;
      this.title = title;
    }
  }

  static class TitleRequest implements Callable<Link> {
    private String mUrl;

    TitleRequest(String url) {
      mUrl = url;
    }

    @Override
    public Link call() throws Exception {
      String title = NO_TITLE;
      try {
        Document doc = Jsoup.connect(mUrl).userAgent(USER_AGENT).get();
        title = doc.title();
      } catch(Exception e) {
        Log.e(LOG_TAG, "Error fetching title", e);
      }
      return new Link(mUrl, title);
    }
  }

  @Override
  public List<Link> extractMatches(String chat) {
    // Find anything that starts with http or https and match until a space
    List<String> matches = ParseUtil.applyRegex(chat, REGEX);
    List<Link> links = new ArrayList<>();

    ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
    List<Future<Link>> futures = new ArrayList<>();

    // request each title
    for (String match : matches) {
      futures.add(executor.submit(new TitleRequest(match)));
    }

    // tell the executor we're done submitting new requests
    executor.shutdown();

    try {
      // retrieve each Link object and add to the return list
      executor.awaitTermination(TIMEOUT_SECONDS, TimeUnit.SECONDS);

      for(Future<Link> future : futures) {
        try {
          links.add(future.get());
        } catch(ExecutionException e) {
          Log.e(LOG_TAG, "Error fetching title", e);
        }
      }
    } catch(InterruptedException e) {
      Log.e(LOG_TAG, "Interrupted while waiting for executor", e);
    }

    // done
    return links;
  }

  @Override
  public String getJsonFieldName() {
    return "links";
  }
}
