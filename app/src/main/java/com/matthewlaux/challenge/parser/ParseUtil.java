package com.matthewlaux.challenge.parser;

import java.util.List;
import java.util.ArrayList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mlaux on 3/27/15.
 */
public class ParseUtil {
  /**
   * Returns the matches of the given Pattern in the given String.
   * If a capture group is included in the pattern, that group is used for the returned matches.
   * Otherwise, the entire matches are returned.
   *
   * @param str     the string to search
   * @param pattern the Pattern to search with
   */
  public static List<String> applyRegex(String str, Pattern pattern) {
    Matcher matcher = pattern.matcher(str);
    List<String> results = new ArrayList<String>();
    while (matcher.find()) {
      if (matcher.groupCount() != 0) {
        // retrieve the captured group and add to list
        results.add(matcher.group(1));
      } else {
        // no captured group in the regex. just add the whole match
        results.add(matcher.group(0));
      }
    }
    return results;
  }
}
