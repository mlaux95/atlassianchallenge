package com.matthewlaux.challenge.parser;

import java.util.List;

/**
 * Created by mlaux on 3/27/15.
 */
interface Parser<ReturnType> {
  public List<ReturnType> extractMatches(String chat);

  /** How this Parser's output will be serialized in the JSON string. */
  public String getJsonFieldName();
}
