package com.matthewlaux.challenge.parser;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.matthewlaux.challenge.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlaux on 4/1/15.
 */
public class ParserTask extends AsyncTask<String, Void, String> {
  /**
   * A list of the Parsers we want to apply to each chat message
   */
  private static final List<Parser<?>> PARSERS = new ArrayList<Parser<?>>();

  /**
   * Create a GSON instance to use. Instances are thread safe.
   */
  private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

  static {
    PARSERS.add(new MentionParser());
    PARSERS.add(new EmoticonParser());
    PARSERS.add(new LinkParser());
  }

  /**
   * Don't prevent the activity from being garbage collected if it's closed
   * while this task is in progress.
   */
  private WeakReference<Activity> mWeakActivity;

  public ParserTask(Activity act) {
    mWeakActivity = new WeakReference<>(act);
  }

  @Override
  protected String doInBackground(String... args) {
    String chat = args[0];
    JsonObject obj = new JsonObject();

    for (Parser parser : PARSERS) {
      List<?> result = parser.extractMatches(chat);
      if (result.isEmpty()) {
        continue;
      }

      JsonElement element = GSON.toJsonTree(result);
      obj.add(parser.getJsonFieldName(), element);
    }

    return GSON.toJson(obj);
  }

  @Override
  protected void onPostExecute(String result) {
    Activity activity = mWeakActivity.get();
    if(activity != null && !activity.isFinishing()) {
      TextView resultView = (TextView) activity.findViewById(R.id.results);
      resultView.setText(result);
    } else {
      Log.e(getClass().getName(), "Activity died while parsing chat.");
    }
  }
}
