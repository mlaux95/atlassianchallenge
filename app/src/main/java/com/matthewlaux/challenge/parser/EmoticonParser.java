package com.matthewlaux.challenge.parser;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by mlaux on 3/27/15.
 */
public class EmoticonParser implements Parser<String> {
  private static final Pattern REGEX = Pattern.compile("\\((\\w{1,15})\\)");

  @Override
  public List<String> extractMatches(String chat) {
    // I looked at the list of HipChat emoticons, and noticed none of them use non-alphanumeric characters.
    // So all I allowed here are word characters. If we wanted to allow symbols, we could modify the expression.

    // Find occurrences of '(' followed by 1-15 word characters and then a ')'. Keep the value inside the
    // parentheses as a matched group
    return ParseUtil.applyRegex(chat, REGEX);
  }

  @Override
  public String getJsonFieldName() {
    return "emoticons";
  }
}
